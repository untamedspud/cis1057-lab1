#ifndef CIS1057_H
#define CIS1057_H

#define PROGRAMMER_NAME     "Ray Lauff"           // your name here
#define ORGANIZATION        "Temple University"
#define LAB                 "Computer Science"
#define ASSIGNED_CLASS      "CIS 1057 Spring 2019"
#define EMAIL_ADDRESS       "labwork@temple.edu"  // your email here

#define YARDS_IN_MILE       1760
#define DEGREES_IN_CIRCLE   360
#define NUMBER_OF_STATES    50
#define ONE_CM_IN_INCHES    0.3937
#define ONE_INCH_IN_CM      2.54	

#define MASCOT_TWITTER      "@StellaEPZ"
#define MASCOT              "Stella the Owl"

const char *AUTHOR  = "Ray Lauff";               // your name here
const char *ORG     = "Temple University";
const char *CLASS   = "CIS 1057 Spring 2019";

const int AVERAGE_DISTANCE_EARTH_TO_MOON_IN_KILOMETERS = 384400;
const int DAYS_IN_JANUARY                              = 31;
const int DAYS_IN_FEBRUARY                             = 28;
const int DAYS_IN_MARCH                                = 31;

// 45678901234567890123456789012345678901234567890123456789012345678901234556789
//       11111111112222222222333333333344444444445555555555666666666677777777777
#endif // CIS1057_H 

