#include <stdio.h>
#include <stdlib.h>
#include "cis1057.h"

/*
 * Programmer:    << put your name here >>
 * Class:         Introduction to C Programming 1057  Spring 2019 Section 004
 * Assignment:    Number 1 – Create my header file
 * Date:          << put today's date here >>
 * Version:       1
 * Description:   Create and test out our header file.
 * File:          lab1.c
 */

int main()
{
	puts( "Hello World!" );
	puts( "GO OWLS" );
	puts( "\n\n");

	char *msg = "This program was created by " 
	    PROGRAMMER_NAME " at " ORGANIZATION " for the " LAB 
	    " department.";
	char *created_string = msg;
	
	puts( created_string );
	puts( "(c) 2019 " PROGRAMMER_NAME );

	return EXIT_SUCCESS;
}
